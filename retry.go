package util

import (
	"math/rand/v2"
	"time"
)

// Retry 重试
func Retry(attempt uint, sleep time.Duration, fn func() error) error {
	if err := fn(); err != nil {
		if attempt--; attempt > 0 {
			sleep += (time.Duration(rand.Int64N(int64(sleep)))) / 2
			time.Sleep(sleep)
			return Retry(attempt, 2*sleep, fn)
		}
		return err
	}
	return nil
}

package util

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"math"
	"runtime"
	"strings"
	"sync"
	"time"
)

var (
	ErrWrongSql     = errors.New("wrong sql")
	ErrEmptyData    = errors.New("empty data")
	ErrWrongDsn     = errors.New("wrong mysql dsn")
	ErrPageTooLarge = errors.New("page too large")
)

// mysqlDBs 存放连接
type mysqlDBs struct {
	kv map[string]*sql.DB
	l  sync.RWMutex
}

// MysqlConnOption 单库连接配置
type MysqlConnOption struct {
	Dsn             string
	Addr            string
	DBName          string
	MaxOpenConn     int
	MaxIdleConn     int
	ConnMaxIdleTime time.Duration
	ConnMaxLifetime time.Duration
}

// MysqlConnParam 连接配置
type MysqlConnParam struct {
	Master *MysqlConnOption
	Slaves []*MysqlConnOption
}

// MysqlLog 操作日志
type MysqlLog struct {
	Err    error
	When   string
	Query  string
	Params []any
}

// Mysql 连接实例
type Mysql struct {
	ConnParam     *MysqlConnParam
	Logger        *Queue
	Listener      func(err error, query string, param ...any)
	dbs           *mysqlDBs
	TimeoutInsert time.Duration
	TimeoutSelect time.Duration
	TimeoutUpdate time.Duration
	TimeoutTrans  time.Duration
	MasterAsSlave bool
}

// NewMysqlConnOption 得到一个单库连接配置
func NewMysqlConnOption(dsn string, maxOpen, maxIdle int, maxIdleTime, maxLifeTime time.Duration) (*MysqlConnOption, error) {
	addr := ""
	dbName := ""
	if cfg, err := mysql.ParseDSN(dsn); err != nil {
		return nil, err
	} else if cfg.Addr == "" || cfg.DBName == "" {
		return nil, ErrWrongDsn
	} else {
		addr = cfg.Addr
		dbName = cfg.DBName
	}
	if maxOpen <= 0 {
		maxOpen = runtime.NumCPU()*2 + 1
	}
	if maxIdle <= 0 {
		maxIdle = maxOpen / 2
		if maxIdle < 2 {
			maxIdle = 2
		}
	}
	if maxIdleTime < 0 {
		maxIdleTime = 3 * time.Minute
	}
	if maxLifeTime < 0 {
		maxLifeTime = 30 * time.Minute
	}
	return &MysqlConnOption{
		Dsn:             dsn,
		Addr:            addr,
		DBName:          dbName,
		MaxOpenConn:     maxOpen,
		MaxIdleConn:     maxIdle,
		ConnMaxIdleTime: maxIdleTime,
		ConnMaxLifetime: maxLifeTime,
	}, nil
}

// NewMysql 得到一个连接实例
func NewMysql(param *MysqlConnParam) *Mysql {
	logger := NewQueue(20)
	return &Mysql{
		ConnParam:     param,
		MasterAsSlave: len(param.Slaves) == 0,
		TimeoutSelect: 3 * time.Second,
		TimeoutInsert: 3 * time.Second,
		TimeoutUpdate: 3 * time.Second,
		TimeoutTrans:  5 * time.Second,
		Logger:        logger,
		dbs: &mysqlDBs{
			kv: make(map[string]*sql.DB),
		},
	}
}

// NewMysqlEasy 快速创建实例
func NewMysqlEasy(dsn ...string) (*Mysql, error) {
	if len(dsn) == 0 {
		return nil, ErrWrongDsn
	}
	master, err := NewMysqlConnOption(dsn[0], 0, 0, 0, 0)
	if err != nil {
		return nil, err
	}
	slaves := make([]*MysqlConnOption, 0)
	for _, s := range dsn[1:] {
		if sl, e := NewMysqlConnOption(s, 0, 0, 0, 0); e == nil {
			slaves = append(slaves, sl)
		}
	}
	return NewMysql(&MysqlConnParam{
		Master: master,
		Slaves: slaves,
	}), nil
}

// SetListener 设置监听函数
func (m *Mysql) SetListener(fn func(err error, query string, param ...any)) {
	m.Listener = fn
}

// CloseDB 关闭连接
func (m *Mysql) CloseDB() {
	m.dbs.l.Lock()
	defer m.dbs.l.Unlock()
	connOptions := append(m.ConnParam.Slaves, m.ConnParam.Master)
	for _, co := range connOptions {
		k := co.Addr + ":" + co.DBName
		if db, ok := m.dbs.kv[k]; ok {
			if db != nil {
				_ = db.Close()
			}
			delete(m.dbs.kv, k)
		}
	}
}

// GetDB 获取一个连接
func (m *Mysql) GetDB(useSlave bool) (*sql.DB, error) {
	if useSlave && m.MasterAsSlave {
		useSlave = false
	}
	var p *MysqlConnOption
	var err error
	if !useSlave {
		p = m.ConnParam.Master
	} else if l := len(m.ConnParam.Slaves); l > 0 {
		p = m.ConnParam.Slaves[RandInt(l)]
	} else {
		p = m.ConnParam.Slaves[0]
	}
	if p == nil || p.Dsn == "" || p.Addr == "" || p.DBName == "" {
		return nil, ErrWrongDsn
	}
	k := p.Addr + ":" + p.DBName
	m.dbs.l.RLock()
	db, ok := m.dbs.kv[k]
	m.dbs.l.RUnlock()
	if ok && db != nil {
		return db, nil
	}
	m.dbs.l.Lock()
	defer m.dbs.l.Unlock()
	if db, ok = m.dbs.kv[k]; ok && db != nil {
		return db, nil
	}
	if db, err = sql.Open("mysql", p.Dsn); err != nil {
		return nil, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), m.TimeoutSelect)
	defer cancel()
	if err = db.PingContext(ctx); err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(p.MaxOpenConn)
	db.SetMaxIdleConns(p.MaxIdleConn)
	db.SetConnMaxIdleTime(p.ConnMaxIdleTime)
	db.SetConnMaxLifetime(p.ConnMaxLifetime)
	if !ok {
		m.dbs.kv[k] = db
	}
	return db, nil
}

// Truncate 清空一张表
func (m *Mysql) Truncate(table string) error {
	db, err := m.GetDB(false)
	if err != nil {
		return err
	}
	q := "TRUNCATE TABLE `" + table + "`"
	ctx, cancel := context.WithTimeout(context.Background(), m.TimeoutUpdate)
	defer cancel()
	defer func() {
		go m.AddLog(q, nil, err)
	}()
	_, err = db.ExecContext(ctx, q)
	return err
}

// InsertDuplicate 插入，如果存在就更新
func (m *Mysql) InsertDuplicate(table string, row *MysqlRow) (int64, error) {
	if row == nil || *row == nil || len(*row) == 0 {
		return 0, ErrEmptyData
	}
	cols := make([]string, 0)
	phds := make([]string, 0)
	sets := make([]string, 0)
	params := make([]any, 0)
	for col, param := range *row {
		cols = append(cols, col)
		params = append(params, param)
		phds = append(phds, "?")
		sets = append(sets, "`"+col+"` = ?")
	}
	params = append(params, params...)
	query := "INSERT INTO `" + table + "` (`" + StrImplode("`, `", cols...) + "`) VALUES (" + StrImplode(", ", phds...) + ") "
	query += "ON DUPLICATE KEY UPDATE " + StrImplode(", ", sets...)
	db, err := m.GetDB(false)
	if err != nil {
		return 0, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), m.TimeoutInsert)
	defer cancel()
	defer func() {
		go m.AddLog(query, params, err)
	}()
	if res, err := db.ExecContext(ctx, query, params...); err != nil {
		return 0, err
	} else {
		return res.LastInsertId()
	}
}

// InsertIgnore 插入，如果存在就忽略
func (m *Mysql) InsertIgnore(table string, row *MysqlRow) (int64, error) {
	return m.Insert(table, row, true)
}

// InsertReplace 插入，如果存在就替换
func (m *Mysql) InsertReplace(table string, row *MysqlRow) (int64, error) {
	return m.Insert(table, row, false, true)
}

// Insert 插入一条数据
func (m *Mysql) Insert(table string, row *MysqlRow, ignoreOrReplace ...bool) (int64, error) {
	ignore := false
	replace := false
	switch len(ignoreOrReplace) {
	case 0:
		ignore = false
		replace = false
	case 1:
		ignore = ignoreOrReplace[0]
		replace = false
	case 2:
		ignore = ignoreOrReplace[0]
		replace = ignoreOrReplace[1]
	default:
		ignore = ignoreOrReplace[0]
		replace = ignoreOrReplace[1]
	}
	if row == nil || *row == nil || len(*row) == 0 {
		return 0, ErrEmptyData
	}
	cols := make([]string, 0)
	phds := make([]string, 0)
	params := make([]any, 0)
	for col, param := range *row {
		cols = append(cols, col)
		params = append(params, param)
		phds = append(phds, "?")
	}
	query := ""
	if ignore {
		query = "INSERT IGNORE "
	} else if replace {
		query = "REPLACE "
	} else {
		query = "INSERT "
	}
	query += "INTO `" + table + "` (`" + StrImplode("`, `", cols...) + "`) VALUES (" + StrImplode(", ", phds...) + ")"
	db, err := m.GetDB(false)
	if err != nil {
		return 0, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), m.TimeoutInsert)
	defer cancel()
	defer func() {
		go m.AddLog(query, params, err)
	}()
	if res, err := db.ExecContext(ctx, query, params...); err != nil {
		return 0, err
	} else {
		return res.LastInsertId()
	}
}

// InsertBatch 批量插入
func (m *Mysql) InsertBatch(table string, rows []*MysqlRow) (ret int64, err error) {
	if len(rows) == 0 {
		return 0, ErrEmptyData
	}
	cols := make([]string, 0)
	phds := make([]string, 0)
	colsReal := make([]string, 0)
	for col := range *(rows[0]) {
		cols = append(cols, col)
		colsReal = append(colsReal, col)
		phds = append(phds, "?")
	}
	nc := len(cols)
	if nc == 0 {
		return 0, ErrEmptyData
	}
	query := "INSERT IGNORE INTO `" + table + "` (`" + StrImplode("`, `", cols...) + "`) VALUES (" + StrImplode(", ", phds...) + ")"
	db, err := m.GetDB(false)
	if err != nil {
		return 0, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), m.TimeoutInsert)
	defer cancel()
	stmt, err := db.PrepareContext(ctx, query)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()
	logs := make([]any, 0)
	defer func() {
		go m.Logger.Push(logs...)
	}()
	for _, row := range rows {
		params := make([]any, 0)
		for _, col := range colsReal {
			params = append(params, (*row)[col])
		}
		if res, err := stmt.ExecContext(ctx, params...); err == nil {
			if affected, err := res.RowsAffected(); err == nil {
				ret += affected
			} else {
				return ret, err
			}
		} else {
			return ret, err
		}
		logs = append(logs, &MysqlLog{
			When:   TimeNowNice(),
			Query:  query,
			Params: params,
			Err:    err,
		})
	}
	return ret, nil
}

// Select 查询数据
func (m *Mysql) Select(query string, params ...any) (rows []*MysqlRow, err error) {
	err = m.SelWalk(func(row *MysqlRow) error {
		rows = append(rows, row)
		return nil
	}, query, params...)
	return
}

// SelPage 按页查询
func (m *Mysql) SelPage(page, pageSize int64, table, where, order, cols string, fn func(row *MysqlRow) error, params ...any) (totalItem, totalPage, curPage int64, err error) {
	if StrTrim(where) == "" {
		where = "1 = 1"
	}
	qCount := "SELECT COUNT(*) FROM `" + table + "` WHERE " + where
	totalItem, err = m.Count(qCount, params...)
	if err != nil {
		return
	}
	if totalItem == 0 {
		totalPage = 0
		curPage = 1
		return
	}
	if pageSize < 1 {
		pageSize = 1
	}
	totalPage = int64(math.Ceil(float64(totalItem) / float64(pageSize)))
	if page < 1 {
		page = 1
	}
	curPage = page
	if curPage > totalPage {
		err = ErrPageTooLarge
		return
	}
	if StrTrim(cols) == "" {
		cols = "*"
	}
	if StrTrim(order) != "" {
		order = " ORDER BY " + order
	} else {
		order = ""
	}
	qSelect := "SELECT " + cols + " FROM `" + table + "` WHERE " + where + order + " LIMIT ?, ?"
	params = append(params, (curPage-1)*pageSize, pageSize)
	err = m.SelWalk(fn, qSelect, params...)
	return
}

// ColumnsWalk 查询数据表结构
func (m *Mysql) ColumnsWalk(fn func(row *MysqlRow) error, table string) error {
	return m.SelWalk(fn, fmt.Sprintf("SHOW COLUMNS FROM `%s`", table))
}

// AllColumns 表所有的字段
func (m *Mysql) AllColumns(table string) ([]string, error) {
	re := make([]string, 0)
	if err := m.ColumnsWalk(func(row *MysqlRow) error {
		re = append(re, row.ToStr("Field"))
		return nil
	}, table); err != nil {
		return nil, err
	}
	return re, nil
}

// ColumnTypes 字段对应类型
func (m *Mysql) ColumnTypes(table string) (map[string]string, error) {
	re := make(map[string]string)
	if err := m.ColumnsWalk(func(row *MysqlRow) error {
		re[row.ToStr("Field")] = row.ToStr("Type")
		return nil
	}, table); err != nil {
		return nil, err
	}
	return re, nil
}

// SelWalk 查询并逐条处理
func (m *Mysql) SelWalk(fn func(row *MysqlRow) error, query string, params ...any) error {
	if (strings.ToLower(query[0:7]) != "select " && strings.ToLower(query[0:5]) != "show ") || len(query) < 8 {
		return ErrWrongSql
	}
	db, err := m.GetDB(true)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), m.TimeoutSelect)
	defer cancel()
	defer func() {
		go m.AddLog(query, params, err)
	}()
	rows, err := db.QueryContext(ctx, query, params...)
	if err != nil {
		return err
	}
	defer rows.Close()
	cols, err := rows.Columns()
	if err != nil {
		return err
	}
	colsLen := len(cols)
	for rows.Next() {
		rawBuffers := make([]sql.RawBytes, colsLen)
		scanArgs := make([]any, colsLen)
		for i := range rawBuffers {
			scanArgs[i] = &rawBuffers[i]
		}
		if err = rows.Scan(scanArgs...); err != nil {
			return err
		}
		row := &MysqlRow{}
		for i, bs := range rawBuffers {
			(*row)[cols[i]] = string(bs)
		}
		if err = fn(row); err != nil {
			return err
		}
	}
	return nil
}

// GetOne 按ID取一条数据
func (m *Mysql) GetOne(table, pk, id string, cols ...string) (*MysqlRow, error) {
	qCols := "*"
	if len(cols) > 0 {
		qCols = "`" + StrImplode("`, `", cols...) + "`"
	}
	if StrTrim(pk) == "" {
		pk = "id"
	}
	rows, err := m.Select("SELECT "+qCols+" FROM `"+table+"` WHERE `"+pk+"` = ? LIMIT 1", id)
	if err != nil {
		return nil, err
	}
	if len(rows) > 0 {
		return rows[0], nil
	}
	return nil, nil
}

// IdsWalk 按多个ID取数据并逐条处理
func (m *Mysql) IdsWalk(fn func(row *MysqlRow) error, table string, pk string, ids []any, cols ...string) error {
	idsNum := len(ids)
	if idsNum == 0 {
		return ErrEmptyData
	}
	queryCols := "*"
	if len(cols) > 0 {
		queryCols = "`" + StrImplode("`, `", cols...) + "`"
	}
	phds := strings.TrimRight(strings.Repeat(" ?,", idsNum), ",")
	if StrTrim(pk) == "" {
		pk = "id"
	}
	return m.SelWalk(func(row *MysqlRow) error {
		return fn(row)
	}, "SELECT "+queryCols+" FROM `"+table+"` WHERE `"+pk+"` IN ("+phds+" )", ids...)
}

// GetByIds 按多个ID取数据
func (m *Mysql) GetByIds(table, pk string, ids []any, cols ...string) (rows []*MysqlRow, err error) {
	switch len(ids) {
	case 0:
		err = ErrEmptyData
		return
	case 1:
		if row, err := m.GetOne(table, pk, InterfaceToStr(ids[0]), cols...); err != nil {
			return rows, err
		} else if row != nil {
			rows = append(rows, row)
			return rows, nil
		}
		return
	default:
		err = m.IdsWalk(func(row *MysqlRow) error {
			rows = append(rows, row)
			return nil
		}, table, pk, ids, cols...)
		return
	}
}

// Count 查询数据结果个数
func (m *Mysql) Count(query string, params ...any) (cnt int64, err error) {
	if strings.ToLower(query[0:13]) != "select count(" || len(query) < 14 {
		return 0, ErrWrongSql
	}
	db, err := m.GetDB(true)
	if err != nil {
		return 0, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), m.TimeoutSelect)
	defer cancel()
	defer func() {
		go m.AddLog(query, params, err)
	}()
	err = db.QueryRowContext(ctx, query, params...).Scan(&cnt)
	return
}

// Update 更新数据
func (m *Mysql) Update(query string, params ...any) (int64, error) {
	if sqlStart7 := strings.ToLower(query[0:7]); (sqlStart7 != "update " && sqlStart7 != "delete ") || len(query) < 8 {
		return 0, ErrWrongSql
	}
	db, err := m.GetDB(false)
	if err != nil {
		return 0, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), m.TimeoutUpdate)
	defer cancel()
	defer func() {
		go m.AddLog(query, params, err)
	}()
	if res, err := db.ExecContext(ctx, query, params...); err != nil {
		return 0, err
	} else if affected, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return affected, nil
	}
}

// UpdateById 按ID更新数据
func (m *Mysql) UpdateById(table string, row *MysqlRow, pk, id string) (int64, error) {
	if row == nil || *row == nil {
		return 0, ErrEmptyData
	}
	colsNum := len(*row)
	id = StrTrim(id)
	if colsNum == 0 || id == "" {
		return 0, ErrEmptyData
	}
	sets := make([]string, 0)
	params := make([]any, 0)
	if StrTrim(pk) == "" {
		pk = "id"
	}
	for col, param := range *row {
		if col == pk {
			continue
		}
		sets = append(sets, "`"+col+"` = ?")
		params = append(params, param)
	}
	if len(sets) == 0 {
		return 0, ErrEmptyData
	}
	params = append(params, id)
	return m.Update("UPDATE `"+table+"` SET "+StrImplode(", ", sets...)+" WHERE `"+pk+"` = ?", params...)
}

// Delete 删除数据
func (m *Mysql) Delete(query string, params ...any) (int64, error) {
	if strings.ToLower(query[0:7]) != "delete " || len(query) < 8 {
		return 0, ErrWrongSql
	}
	return m.Update(query, params...)
}

// DelByIds 按一个或多个ID删除数据
func (m *Mysql) DelByIds(table, pk string, ids ...any) (i int64, err error) {
	idsNum := len(ids)
	if idsNum == 0 {
		return 0, ErrEmptyData
	}
	if StrTrim(pk) == "" {
		pk = "id"
	}
	if idsNum == 1 {
		if ids[0] == "" {
			return 0, ErrEmptyData
		}
		i, err = m.Delete("DELETE FROM `"+table+"` WHERE `"+pk+"` = ?", ids[0])
	} else {
		phds := strings.TrimRight(strings.Repeat(" ?,", idsNum), ",")
		i, err = m.Delete("DELETE FROM `"+table+"` WHERE `"+pk+"` IN ("+phds+" )", ids...)
	}
	return
}

// Transaction 事务
func (m *Mysql) Transaction(fn func(tx *sql.Tx, addLog func(query string, params []any, err error)) error, opts *sql.TxOptions) error {
	db, err := m.GetDB(false)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), m.TimeoutTrans)
	defer cancel()
	tx, err := db.BeginTx(ctx, opts)
	if err != nil {
		return err
	}
	defer func() {
		if p := recover(); p != nil {
			_ = tx.Rollback()
			panic(p)
		}
		if err != nil {
			_ = tx.Rollback()
		}
	}()
	if err = fn(tx, m.AddLog); err == nil {
		err = tx.Commit()
	}
	return err
}

// AddLog 添加日志
func (m *Mysql) AddLog(query string, params []any, err error) {
	if m.Listener != nil {
		m.Listener(err, query, params...)
		return
	}
	m.Logger.Push(&MysqlLog{
		When:   TimeNowNice(),
		Query:  query,
		Params: params,
		Err:    err,
	})
}

// PopLogs 处理日志
func (m *Mysql) PopLogs(fn func(ql *MysqlLog) error) error {
	for {
		if log, _ := m.Logger.Pop(); log == nil {
			break
		} else if ql, ok := log.(*MysqlLog); ok {
			if err := fn(ql); err != nil {
				return err
			}
		}
	}
	return nil
}

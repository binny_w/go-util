package util

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cast"
	"math/rand/v2"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"reflect"
	"runtime"
	"syscall"
)

// ShellExecWalk 逐行处理命令执行结果
func ShellExecWalk(fn func(s string) error, cmdBin string, args ...string) error {
	var out bytes.Buffer
	cmd := exec.Command(cmdBin, args...)
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		return err
	}
	sc := bufio.NewScanner(&out)
	for sc.Scan() {
		if err := fn(sc.Text()); err != nil {
			return err
		}
	}
	return nil
}

// ShellExec 执行命令
func ShellExec(cmdBin string, args ...string) (ret []string, err error) {
	err = ShellExecWalk(func(s string) error {
		ret = append(ret, s)
		return nil
	}, cmdBin, args...)
	return
}

// RandInt 获得随机数
func RandInt(i int) int {
	if i <= 1 {
		return 0
	}
	return rand.IntN(i)
}

// RandRange 按范围取随机值 [min, max)
func RandRange(min, max int) int {
	if max <= min {
		min, max = max, min
	}
	return RandInt(max-min) + min
}

// LocalIp 获取内网 IP
func LocalIp() string {
	as, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range as {
		if in, ok := address.(*net.IPNet); ok && !in.IP.IsLoopback() {
			if in.IP.To4() != nil {
				return in.IP.String()
			}
		}
	}
	return ""
}

// InArray needle 是否存在于 haystack 中
func InArray(needle any, haystack any) (isIn bool, err error) {
	val := reflect.ValueOf(haystack)
	switch val.Kind() {
	case reflect.Slice, reflect.Array:
		for i := 0; i < val.Len(); i++ {
			if reflect.DeepEqual(needle, val.Index(i).Interface()) {
				isIn = true
				return
			}
		}
		return
	case reflect.Map:
		for _, k := range val.MapKeys() {
			if reflect.DeepEqual(needle, val.MapIndex(k).Interface()) {
				isIn = true
				return
			}
		}
		return
	default:
		err = fmt.Errorf("haystack: haystack must be slice, array or map")
		return
	}
}

// InterfaceToInt interface 转为 int
func InterfaceToInt(v any) (ret int) {
	ret = cast.ToInt(v)
	return
}

// InterfaceToInt64 interface 转为 int64
func InterfaceToInt64(v any) (ret int64) {
	ret = cast.ToInt64(v)
	return
}

// InterfaceToFloat64 interface 转为 float64
func InterfaceToFloat64(v any) (ret float64) {
	ret = cast.ToFloat64(v)
	return
}

// InterfaceToStr interface 转为 string
func InterfaceToStr(v any) (ret string) {
	ret = cast.ToString(v)
	return
}

// VarDump 打印变量
func VarDump(v ...any) {
	_, f, l, _ := runtime.Caller(1)
	fmt.Printf("----- %s : %d @ %s -----\n", f, l, TimeNowNice())
	for _, item := range v {
		fmt.Printf("%+v\n", item)
	}
}

// GetBinPath 取可执行程序的绝对路径
func GetBinPath() (string, error) {
	return filepath.Abs(filepath.Dir(os.Args[0]))
}

// GetHomeDir 获取当前用户的Home根目录路径
func GetHomeDir() (string, error) {
	return homedir.Dir()
}

// WaitQuit 运行时等待中止信号
func WaitQuit(f func()) {
	if f != nil {
		defer f()
	}
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
}

// Restart 重启当前程序（实验中）
func Restart(fn func()) error {
	// 获取当前程序的路径
	cmdPath, err := os.Executable()
	if err != nil {
		return err
	}
	// 创建一个新的命令来重新启动当前程序
	cmd := exec.Command(cmdPath)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = os.Environ() // 传递当前的环境变量
	if err = cmd.Start(); err != nil {
		return err
	}
	// 等待新进程完成
	if err = cmd.Wait(); err != nil {
		return err
	}
	// 退出当前程序
	if fn != nil {
		fn()
	}
	os.Exit(0)

	return nil
}

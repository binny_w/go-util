package util

import "time"

const (
	_dateTimeAsSN   = "20060102150405"
	_dateTimeFormat = "2006-01-02 15:04:05"
	_dateFormat     = "2006-01-02"
	_timeFormat     = "15:04:05"
)

// TimeLocationBJ 北京、上海、重庆时区
func TimeLocationBJ() *time.Location {
	return time.FixedZone("CST", 28800)
}

// TimeNice 按常用格式输出日期时间
func TimeNice(t time.Time) string {
	return t.In(TimeLocationBJ()).Format(_dateTimeFormat)
}

// TimeNowNice 输出当前日期时间
func TimeNowNice() string {
	return time.Now().In(TimeLocationBJ()).Format(_dateTimeFormat)
}

// TimeNowAsSN 输出当前日期时间
func TimeNowAsSN() string {
	return time.Now().In(TimeLocationBJ()).Format(_dateTimeAsSN)
}

// TimeNowDateNice 输出当前日期
func TimeNowDateNice() string {
	return time.Now().In(TimeLocationBJ()).Format(_dateFormat)
}

// TimeNowTimeNice 输出当前时间
func TimeNowTimeNice() string {
	return time.Now().In(TimeLocationBJ()).Format(_timeFormat)
}

// TimeFormStamp 时间戳转时间
func TimeFormStamp(ts int64) string {
	return time.Unix(ts, 0).In(TimeLocationBJ()).Format(_dateTimeFormat)
}

// TimeDateFromStamp 时间戳转日期
func TimeDateFromStamp(ts int64) string {
	return time.Unix(ts, 0).In(TimeLocationBJ()).Format(_dateFormat)
}

// TimeToStamp 时间转时间戳
func TimeToStamp(str string) (int64, error) {
	if t, err := time.ParseInLocation(_dateTimeFormat, str, TimeLocationBJ()); err != nil {
		return 0, err
	} else {
		return t.Unix(), nil
	}
}

// TimeDateToStamp 日期转时间戳
func TimeDateToStamp(str string) (int64, error) {
	if t, err := time.ParseInLocation(_dateFormat, str, TimeLocationBJ()); err != nil {
		return 0, err
	} else {
		return t.Unix(), nil
	}
}

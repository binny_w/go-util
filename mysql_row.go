package util

// MysqlRow 数据单元
type MysqlRow map[string]any

// ToStr 查询结果转字符串
func (row *MysqlRow) ToStr(key string) string {
	return InterfaceToStr((*row)[key])
}

func (row *MysqlRow) ToStrings(key string) []string {
	ret, _ := (*row)[key].([]string)
	return ret
}

func (row *MysqlRow) ToMap(key string) map[string]any {
	ret, _ := (*row)[key].(map[string]any)
	return ret
}

// ToInt64 查询结果转整形
func (row *MysqlRow) ToInt64(key string) int64 {
	return InterfaceToInt64((*row)[key])
}

// ToNum 查询结果转数字
func (row *MysqlRow) ToNum(key string) float64 {
	return InterfaceToFloat64((*row)[key])
}

func (row *MysqlRow) Drop(keys ...string) {
	for _, key := range keys {
		delete(*row, key)
	}
}

func (row *MysqlRow) DropAll() {
	clear(*row)
}

func (row *MysqlRow) Set(key string, val any) {
	(*row)[key] = val
}

func (row *MysqlRow) Has(key string) bool {
	_, ok := (*row)[key]
	return ok
}

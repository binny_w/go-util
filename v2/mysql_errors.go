package util

import "errors"

var (
	ErrMysqlWrongSql      = errors.New("wrong sql")
	ErrMysqlEmptyData     = errors.New("empty data")
	ErrMysqlWrongDsn      = errors.New("wrong mysql dsn")
	ErrMysqlWrongOption   = errors.New("wrong option")
	ErrMysqlPageTooLarge  = errors.New("page too large")
	ErrMysqlColWithBQuote = errors.New("column name with back quote")
)
